package com.vcint.payment.cashier.test.acceptance.tests.general;

import com.vcint.payment.cashier.test.acceptance.commons.TestBase;
import com.vcint.payment.cashier.test.acceptance.pages.LoginPage;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

//To run: mvn clean test -DsiteUrl="http://test.betvictor.com"

/**
 * Created by dobrosip on 10/5/2015.
 */
public class LoginTest extends TestBase {
    final static Logger logger = Logger.getLogger(LoginTest.class);

    @Test(enabled = false)
    @Parameters({ "userName", "password" })
    public void shouldPerformSuccessfulLogin(String userName, String password)
    {
        String testName = testContext.getName()+":"+
                          this.getClass().getName()+":"+
                          new Object(){}.getClass().getEnclosingMethod().getName();

        logger.debug("Entering: " + testName);
        LoginPage loginPage = new LoginPage(driver);
        logger.info(testName+"#"+"I navigate to environment: " +this.siteUrl);
        logger.info(testName+"#"+"I try to login with user: "+userName);
        boolean result = false;
        try {
            result = loginPage.login(userName, password);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
        logger.info(testName+"#"+"Expected result: Login is successfull, Actual result:"+result);
        Assert.assertTrue(result);
        logger.debug("Exiting: " + testName);
    }
}
