package com.vcint.payment.cashier.test.acceptance.tests.deposit;

import com.vcint.payment.cashier.test.acceptance.commons.DtoHelper;
import com.vcint.payment.cashier.test.acceptance.commons.TestBase;
import com.vcint.payment.cashier.test.acceptance.pages.AccountOverviewPage;
import com.vcint.payment.cashier.test.acceptance.pages.DepositPage;
import com.vcint.payment.cashier.test.acceptance.pages.LoginPage;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Created by dobrosip on 10/7/2015.
 */
public class DepositTest  extends TestBase {
    final static Logger logger = Logger.getLogger(DepositTest.class);

    @Test(enabled = true)
    @Parameters({ "testID", "useExistingCard", "cardNumber", "userName", "password", "csc", "amount" })
    public void shouldPerformSuccessfulDepositWithStoredVisaCard(String testID, String useExistingCard, String cardNumber, String userName, String password, String csc, String amount)
    {
        String testName = testContext.getName()+":"+
                this.getClass().getName()+":"+
                new Object(){}.getClass().getEnclosingMethod().getName();

        logger.debug("Entering: " + testName);
        LoginPage loginPage = new LoginPage(driver);
        logger.info(testName+"#"+testID);
        logger.info(testName+"#"+"I navigate to environment: " +this.siteUrl);
        logger.info(testName+"#"+"I login with user: "+userName);
        try {
            loginPage.login(userName, password);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
        logger.info(testName+"#"+"I open MyAccount");
        loginPage.openMyAccount();
        logger.info(testName+"#"+"I open deposit");
        new AccountOverviewPage(driver).openDeposit();
        DepositPage depositPage = new DepositPage(driver);
        logger.info(testName+"#"+"I perform deposit");
        depositPage.performDeposit(DtoHelper.constructPaymentDto(cardNumber, "", "", "", csc, amount, useExistingCard));
        boolean result = depositPage.getTransactionResult().startsWith("Successful Transaction");
        logger.info(testName+"#"+"Expected result: Deposit was successfull, Actual result:"+result);
        Assert.assertTrue(result);
        logger.debug("Exiting: " + testName);
    }

    @Test(enabled = true)
    @Parameters({ "testID", "useExistingCard", "cardNumber", "nameOnCard", "expiryYear", "expiryMonth", "userName", "password", "csc", "amount" })
    public void shouldPerformSuccessfulDepositWithNewVisaCard(String testID, String useExistingCard, String cardNumber, String nameOnCard, String expiryYear, String expiryMonth, String userName, String password, String csc, String amount)
    {
        String testName = testContext.getName()+":"+
                this.getClass().getName()+":"+
                new Object(){}.getClass().getEnclosingMethod().getName();

        logger.debug("Entering: " + testName);
        LoginPage loginPage = new LoginPage(driver);
        logger.info(testName+"#"+testID);
        logger.info(testName+"#"+"I navigate to environment: " +this.siteUrl);
        logger.info(testName+"#"+"I login with user: "+userName);
        try {
            loginPage.login(userName, password);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
        logger.info(testName+"#"+"I open MyAccount");
        loginPage.openMyAccount();
        logger.info(testName+"#"+"I open deposit");
        new AccountOverviewPage(driver).openDeposit();
        DepositPage depositPage = new DepositPage(driver);
        logger.info(testName+"#"+"I perform deposit");
        depositPage.performDeposit(DtoHelper.constructPaymentDto(cardNumber, nameOnCard, expiryYear, expiryMonth, csc, amount, useExistingCard));
        boolean result = depositPage.getTransactionResult().startsWith("Successful Transaction");
        logger.info(testName+"#"+"Expected result: Deposit was successfull, Actual result:"+result);
        Assert.assertTrue(result);
        logger.debug("Exiting: " + testName);
    }


}
