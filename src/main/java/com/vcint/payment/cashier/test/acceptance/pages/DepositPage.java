package com.vcint.payment.cashier.test.acceptance.pages;

import com.vcint.payment.cashier.test.acceptance.commons.DatePicker;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;
import java.util.Map;
import com.google.common.base.Function;

/**
 * Created by dobrosip on 10/7/2015.
 */
public class DepositPage  extends PageBase {
    private final static Logger logger = Logger.getLogger(DepositPage.class);
    public static final String CASHIER_FRAME = "cashier_frame";

    @FindBy(id = "bcDropdownMenu")
    public WebElement bankCardDropdownMenu;

    @FindBy(name = "csc")
    public WebElement csc;

    @FindBy(xpath = "//*/input[@id='amount']")
    public WebElement amount;

    @FindBy(id = "submitBtn")
    public WebElement submit;

    @FindBy(xpath = "//*[@id='bcDropdownMenu']")
    private List<WebElement> bankCardDropdownMenuOptions;

    @FindBy(partialLinkText = "Add new")
    private WebElement addNewCardLink;

    @FindBy(id = "pan")
    private WebElement cardNumberField;

    @FindBy(id = "nameOnTheCard")
    private WebElement nameOnCardField;


    public DepositPage(WebDriver driver) {
        super(driver);
    }

    public void performDeposit(Map<String, String> paymentData) {
        try {
            populatePageWithData(paymentData);
        } catch (Exception e) {
            logger.error("Exception ocurred in test during page popluation with data:" + paymentData.toString(), e);
            Assert.fail("Exception ocurred in test during page popluation with data:" + paymentData.toString(), e);
        }
        submit();
    }

    private void populatePageWithData(Map<String, String> paymentData) throws Exception {
        selectPaymentMethod("CCMBVT");
        if (Boolean.parseBoolean(paymentData.get("useExistingCard"))) {
            //select existing card
            waitForElementToBeClickable(bankCardDropdownMenu).click();
            waitForElementToBeClickable(getCardElementByPattern(paymentData.get("cardNumber"))).click();
        } else {
            //add new card
            if(driver.findElements(By.id("bcDropdownMenu")).size() > 0)
            {
                //not the first card to be added, other cards already exist
                waitForElementToBeClickable(bankCardDropdownMenu).click();
                waitForElementToBeClickable(addNewCardLink);
                javascriptClick(addNewCardLink);
            }

            fillCardNumberField(paymentData.get("cardNumber"));
            fillStandardField(nameOnCardField, paymentData.get("nameOnCard"));
            new DatePicker(driver).setDate(paymentData.get("expiryYear"), paymentData.get("expiryMonth"));
        }

        waitForElementToBeClickable(this.csc).sendKeys(paymentData.get("csc")+Keys.TAB);
        fillStandardField(this.amount, paymentData.get("amount"));
    }

    public void selectPaymentMethod(String method)
    {
        switch (method){
            case "CCMBVT" : {
                waitForElementToBeClickable(driver.findElement(By.xpath("//*[@title='Credit/Debit Cards BVT']"))).click();
                new WebDriverWait(driver, 30).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(CASHIER_FRAME));
                break;
            }
        }
    }

    private void fillCardNumberField(String cardNumber) {
        waitForElementToBeClickable(cardNumberField).clear();

        for (char digit : (cardNumber).toCharArray()) {
            waitForElementToBeClickable(cardNumberField).sendKeys("" + digit);
        }

        waitForElementToBeClickable(cardNumberField).sendKeys(Keys.TAB);
    }

    public void submit()
    {
        submit.click();
    }

    public String getTransactionResult()
    {
        WebElement success =  new WebDriverWait(driver, 30).until(
                new Function<WebDriver, WebElement>() {
                    public WebElement apply(WebDriver driver) {
                        return driver.findElement(By.cssSelector("div.success"));
                    }
                }
        );
        return success.getText();
    }

    private WebElement getCardElementByPattern(String pattern) {
        WebElement result = null;

        for (WebElement optionElement : bankCardDropdownMenuOptions) {
            WebElement span = optionElement.findElement(By.xpath("span"));
            if (span.getText().equals(pattern)) {
                result = optionElement;
                break;
            }
        }
        return result;
    }


}

