package com.vcint.payment.cashier.test.acceptance.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by dobrosip on 10/5/2015.
 * Base class for PageObjects to define common methods.
 */
public class PageBase {
    protected WebDriver driver = null;

    public PageBase(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void navigateToPage(String url) {
        driver.manage().window().maximize();
        driver.get(url);
        waitForPageLoad();
    }

    public void waitForPageLoad() {
        ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }
        };
        new WebDriverWait(driver, 30).until(pageLoadCondition);
    }

    public WebElement waitForElementToBeClickable(WebElement element) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(element));
        return element;
    }

    public void switchToIframe(String iFrameCss) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(driver.findElement(By.cssSelector(iFrameCss)));
    }

    public void waitForAjaxCallToComplete() {
        new WebDriverWait(driver, 180).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                JavascriptExecutor js = (JavascriptExecutor) driver;
                return (Boolean) js.executeScript("return jQuery.active == 0");
            }
        });
    }

    /*
     * Alternate clicking method, used in certain cases to avoid cross browser issues.
     */
    public void javascriptClick(WebElement element)
    {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", element);
    }

    public void switchToWindow(String windowName)
    {
        for(String winHandle : driver.getWindowHandles()){
            if(driver.switchTo().window(winHandle).getTitle().equals(windowName)) {
                break;
            }
        }
    }

    public void fillStandardField(WebElement field, String value) {
        WebElement fieldElement =  waitForElementToBeClickable(field);
        fieldElement.clear();
        fieldElement.sendKeys(value);
        fieldElement.sendKeys(Keys.TAB);
    }

}