package com.vcint.payment.cashier.test.acceptance.commons;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dobrosip on 10/7/2015.
 */
public class DtoHelper {
    public static Map<String, String> constructPaymentDto(String cardNumber, String nameOnCard, String expiryYear,
                                                          String expiryMonth, String csc, String amount, String useExistingCard) {
        Map<String, String> paymentData = new HashMap<String, String>();
        paymentData.put("useExistingCard", useExistingCard);
        paymentData.put("cardNumber", cardNumber);
        paymentData.put("nameOnCard", nameOnCard);
        paymentData.put("expiryYear", expiryYear);
        paymentData.put("expiryMonth", expiryMonth);
        paymentData.put("csc", csc);
        paymentData.put("amount", amount);
        return paymentData;
    }
}
