package com.vcint.payment.cashier.test.acceptance.pages;

import com.vcint.payment.cashier.test.acceptance.commons.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by dobrosip on 10/5/2015.
 */
public class LoginPage extends PageBase {
    private final static Logger logger = Logger.getLogger(LoginPage.class);
    private final static String PAGE_URL = "/sports/en-gb";
    private final static String MY_ACCOUNT_URL = "/sports/en-gb/views/overview";
    private final static String LOGOUT_URL = "/sports/en-gb/logout";

    @FindBy(id = "username")
    public WebElement userName;

    @FindBy(id = "password")
    public WebElement password;

    @FindBy(className = "submit")
    public WebElement submit;

    public  LoginPage(WebDriver driver)
    {
        super(driver);
    }

    public boolean login(String userName, String password) throws Exception {
        navigateToPage(TestBase.getTargetEnvironment()+PAGE_URL);
        int retry = 0;
        while(isLoggedIn()){
            if(retry>10)
            {
                throw  new Exception("Failed to logout from an already logged in session.");
            }
            logout();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            navigateToPage(TestBase.getTargetEnvironment() + PAGE_URL);
            retry++;
        }
        fillStandardField(this.userName, userName);
        fillStandardField(this.password, password);
        submit.click();
        waitForPageLoad();

        return isLoggedIn();
    }

    public void logout(){
        navigateToPage(TestBase.getTargetEnvironment()+LOGOUT_URL);
        new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOfElementLocated(By.className("logout")));
        waitForPageLoad();
        driver.manage().deleteAllCookies();
    }

    public boolean isLoggedIn(){
        return (driver.findElements(By.className("logout")).size() > 0);
    }

    public void openMyAccount()
    {
        navigateToPage(TestBase.getTargetEnvironment()+MY_ACCOUNT_URL);
        waitForPageLoad();
        switchToWindow("My Account - BetVictor");
        driver.manage().window().maximize();
    }
}
