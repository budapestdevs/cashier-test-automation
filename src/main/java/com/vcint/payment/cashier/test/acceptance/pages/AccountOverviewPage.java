package com.vcint.payment.cashier.test.acceptance.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by dobrosip on 10/7/2015.
 */
public class AccountOverviewPage extends PageBase {
    private final static Logger logger = Logger.getLogger(AccountOverviewPage.class);
    public static final String CASHIER_OUTER_FRAME = "cashier_outer_frame";

    @FindBy(className = "deposit")
    public WebElement deposit;

    public AccountOverviewPage(WebDriver driver) {
        super(driver);
    }

    public void openDeposit(){
        javascriptClick(deposit);
        new WebDriverWait(driver, 30).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(CASHIER_OUTER_FRAME));
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className("bv-select-select")));
    }
}
