package com.vcint.payment.cashier.test.acceptance.commons;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

/**
 * Created by dobrosip on 10/5/2015.
 * Base class for specific tests.
 * Defines before and after hook.
 */
public abstract class TestBase{
    private final static String SCREENSHOT_DIRECTORY = "target/surefire-reports/";
    private final static String SCREENSHOT_FILE_POSTFIX = "_failed.png";
    final static Logger logger = Logger.getLogger(TestBase.class);
    public static final String TEST_BETVICTOR_COM = "test.betvictor.com";
    public static final String HTTPS_INT_CASHIER_BVTPAY_COM_EMP_INTERNAL_STATUS_VERSION = "https://int.cashier.bvtpay.com/emp/internal/status/version";
    protected WebDriver driver = null;
    protected static String siteUrl = "";
    protected static String browser = "";
    protected ITestContext testContext;

    @Parameters({ "chromeDriverLocation", "ieDriverLocation", "browser", "os", "version", "siteUrl", "seleniumGridHubUrl" })
    @BeforeClass
    public void setUp(String chromeDriverLocation, String ieDriverLocation, String browser, String os, String version, String siteUrl,
                      @Optional String seleniumGridHubUrl) {
        Platform platform = null;
        this.siteUrl = siteUrl;
        DesiredCapabilities capabilities = null;
        if (seleniumGridHubUrl != null && !seleniumGridHubUrl.equals("")) {
            // via Selenium Grid
            if (browser.equalsIgnoreCase("firefox")) {
                capabilities = DesiredCapabilities.firefox();
            } else if (browser.equalsIgnoreCase("InternetExplorer")) {
                capabilities = DesiredCapabilities.internetExplorer();
                capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
                        true);
                if (ieDriverLocation != null && !ieDriverLocation.isEmpty()) {
                    System.setProperty("webdriver.ie.driver", ieDriverLocation);
                }
            } else if (browser.equalsIgnoreCase("chrome")) {
                capabilities = DesiredCapabilities.chrome();
                if (chromeDriverLocation != null && !chromeDriverLocation.isEmpty()) {
                    System.setProperty("webdriver.chrome.driver", chromeDriverLocation);
                }
            } else if (browser.equalsIgnoreCase("opera")) {
                capabilities = DesiredCapabilities.opera();
            }

            if (version != null && !version.isEmpty()) {
                capabilities.setVersion(version);
            }

            if (os.equalsIgnoreCase("Windows")) {
                platform = Platform.WINDOWS;
            }

            if (platform != null) {
                capabilities.setPlatform(platform);
            }

            try {
                logger.info("Creating remote driver on HUB: "+seleniumGridHubUrl+ " with "+browser);
                driver = new RemoteWebDriver(new URL(seleniumGridHubUrl), capabilities);
            } catch (MalformedURLException e) {
                logger.error("MalformedURLException: "+seleniumGridHubUrl);
                e.printStackTrace();
            }
        } else {
            // on localhost
            logger.info("Creating local driver "+browser);
            if (browser.equalsIgnoreCase("firefox")) {
                driver = new FirefoxDriver();
            } else if (browser.equalsIgnoreCase("InternetExplorer")) {
                if (ieDriverLocation != null && !ieDriverLocation.isEmpty()) {
                    System.setProperty("webdriver.ie.driver", ieDriverLocation);
                    driver = new InternetExplorerDriver();
                }

            } else if (browser.equalsIgnoreCase("chrome")) {
                if (chromeDriverLocation != null && !chromeDriverLocation.isEmpty()) {
                    System.setProperty("webdriver.chrome.driver", chromeDriverLocation);
                    driver = new ChromeDriver();
                }
            } else if (browser.equalsIgnoreCase("chrome")) {
               driver = new OperaDriver();
            }
        }
        Capabilities cap =  ((RemoteWebDriver)driver).getCapabilities();
        this.browser +=  String.format("%s %s %s", os, cap.getBrowserName(),  cap.getVersion())+"<br>";
        driver.manage().deleteAllCookies();
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @BeforeTest
    public void startTest(final ITestContext testContext) {
            this.testContext = testContext;
    }

    @AfterMethod
    public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
        if (testResult.getStatus() == ITestResult.FAILURE) {
            //TODO: capture fullscreen
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File(
                    String.format("%1$s%2$s%3$s", SCREENSHOT_DIRECTORY, testResult.getTestClass().getXmlTest().getName() +"-"+testResult.getInstanceName() +"-"+testResult.getName(), SCREENSHOT_FILE_POSTFIX)));
        }
    }

    public static String getBrowser(){
        return browser;
    }

    public static String getTargetEnvironment(){
        return siteUrl;
    }

    public static String getApplicationVersion(){
        StringBuilder result = new StringBuilder();
        String cashierUrl = "";
        if(siteUrl.contains(TEST_BETVICTOR_COM)) {
            cashierUrl = HTTPS_INT_CASHIER_BVTPAY_COM_EMP_INTERNAL_STATUS_VERSION;
        }
        URL url = null;
        try {
            url = new URL(cashierUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

}
